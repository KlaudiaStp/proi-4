#ifndef PACMAN_H
#define PACMAN_H
#include <QObject>
#include <QGraphicsPixmapItem>
#include "Game.h"
#include <QTimer>
#include <QKeyEvent>


class Pacman: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Pacman(Game *g);
    void keyPressEvent(QKeyEvent *event);
    void moveTo(int x, int y);
    int getX();
    int getY();

public slots:
    void move();

private:
    int posX;
    int posY;
    int direction;
    void print();
    Game *game;
    QTimer *timer;

};

#endif // PACMAN_H
