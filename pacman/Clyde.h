#ifndef CLYDE_H
#define CLYDE_H
#include "Ghost.h"

class Clyde : public Ghost
{
public:
    Clyde(Game *g, int x, int y);
    virtual void move();

};

#endif // CLYDE_H
