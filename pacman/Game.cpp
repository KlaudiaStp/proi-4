#include "Game.h"
#include "Pacman.h"
#include "Blinky.h"
#include "Pinky.h"
#include <QString>
#include <QFile>
#include <string>
#include <QGraphicsRectItem>
#include <QDebug>
#include "Inky.h"
#include "Clyde.h"

struct Game::GameImpl
{
    int level;
    int lives;
    int score;
    int fruit;
    QGraphicsTextItem *scoreText;
    QGraphicsTextItem *levelText;
    QGraphicsTextItem *livesText;
    Pacman * pacman;
    int board [BOARDH][BOARDW];
};

Game::Game():impl_(new GameImpl)
{
    scene = new QGraphicsScene ();
    scene->setSceneRect(0,0,SCENEW, SCENEH);
    scene->setBackgroundBrush(QBrush(Qt::black));
    setScene(scene);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(SCENEW, SCENEH);
    impl_->level=0;
    impl_->score=0;
    impl_->lives=5;
    gameSet();

}

void Game::play()
{
    show();
}

bool Game::canMove(int x, int y)
{
    bool ok=((x>=0) && (x<BOARDW) && (y>=0) && (y<BOARDH));
    return (!ok)? false: (impl_->board[y][x]!=1);
}

void Game::applyPacmanMove(int type)
{
    impl_->score=impl_->score+10;
    --impl_->fruit;
    if(type==2)
    {
        ++impl_->lives;
    }
    if(impl_->fruit==0)
    {
        gameSet();
    }
    changeDisp();
}

void Game::applyGhostMove(QGraphicsPixmapItem* ghost)
{
    --impl_->lives;
    changeDisp();
    impl_->pacman->moveTo(0,0);
    if(impl_->lives==0)
    {
        QMessageBox message;
        message.setText("Game over");
        message.exec();
        delete this;
    }



}

int Game::getPacmanX()
{
    return impl_->pacman->getX();
}

int Game::getPacmanY()
{
    return impl_->pacman->getY();
}

void Game::gameSet()
{
    ++impl_->level;

    if(impl_->level<=MAX_LEVEL){

   // Fruit *f= new Fruit(5,5,1);
    //scene->addItem(f);
    try
    {


    QString fileName(QString::fromStdString(":/levels/level")+QString::fromStdString(std::to_string(impl_->level))+QString::fromStdString(".txt"));
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)) throw 10;
    QList<QGraphicsItem *>   list = scene->items();
    for(int i=0; i<list.size(); i++)
    {
        QGraphicsItem *item = list.at(i);
        scene->removeItem(item);
        delete item;
    }
    for(int i=0; i<BOARDH; i++)
            for(int j=0; j<BOARDW; j++)
                impl_->board[i][j]=0;

    impl_->fruit;
    for(int i=0; i<BOARDH; i++)
    {
        QString s= file.readLine();
        qDebug()<<s;
        for(int j=0; j<BOARDW; j++)
        {
            if(s.at(j).toLatin1()=='x' ||s.at(j).toLatin1()=='X')
            {
                impl_->board[i][j]=1;
                QGraphicsRectItem *e = new QGraphicsRectItem(j*CELLW, i*CELLH, CELLW, CELLH);
                e->setBrush(QBrush(Qt::blue));
                scene->addItem(e);
            }
            else if(s.at(j).toLatin1()==' ')
            {
                Fruit *f= new Fruit(j,i,1);
                scene->addItem(f);
                ++impl_->fruit;
            }
            else if(s.at(j).toLatin1()=='A')
            {
                Blinky *b = new Blinky(this, j, i);
                scene->addItem(b);
                qDebug()<<"blinky";
            }
            else if(s.at(j).toLatin1()=='B')
            {
                Pinky *p = new Pinky(this, j, i);
                scene->addItem(p);
                qDebug()<<"pinky";
            }
            else if(s.at(j).toLatin1()=='C')
            {
                Inky *in = new Inky(this, j, i);
                scene->addItem(in);
                qDebug()<<"inky";
            }
            else if(s.at(j).toLatin1()=='D')
            {
                Clyde *c = new Clyde(this, j, i);
                scene->addItem(c);
                qDebug()<<"clyde";
            }
            else if(s.at(j).toLatin1()=='F')
            {
                Fruit *f= new Fruit(j,i,2);
                scene->addItem(f);
                ++impl_->fruit;
            }

        }
    }
    impl_->pacman = new Pacman(this);
    scene->addItem(impl_->pacman);
    impl_->levelText = new QGraphicsTextItem();
    impl_->levelText->setPlainText(QString("Level: ") + QString::number(impl_->level));
    impl_->levelText->setDefaultTextColor(Qt::red);
        impl_->levelText->setFont(QFont("times",16));
        impl_->levelText->setPos(4,BOARDH*CELLH+4);
        scene->addItem(impl_->levelText);
        impl_->livesText = new QGraphicsTextItem();
        impl_->livesText->setPlainText(QString("Lives: ") + QString::number(impl_->lives));
        impl_->livesText->setDefaultTextColor(Qt::red);
            impl_->livesText->setFont(QFont("times",16));
            impl_->livesText->setPos(100,BOARDH*CELLH+4);
            scene->addItem(impl_->livesText);

            impl_->scoreText = new QGraphicsTextItem();
            impl_->scoreText->setPlainText(QString("Score: ") + QString::number(impl_->score));
            impl_->scoreText->setDefaultTextColor(Qt::red);
                impl_->scoreText->setFont(QFont("times",16));
                impl_->scoreText->setPos(200,BOARDH*CELLH+4);
                scene->addItem(impl_->scoreText);
    }
    catch(int e)
    {
        QMessageBox message;
        message.setText(QString("Error ")+QString::number(e));
        message.exec();
    }
    }
    else
    {
        QMessageBox message;
        message.setText("No more levels");
        message.exec();
        delete this;

    }
}
void Game::changeDisp()
{
    impl_->levelText->setPlainText(QString("Level: ") + QString::number(impl_->level));
    impl_->livesText->setPlainText(QString("Lives: ") + QString::number(impl_->lives));
    impl_->scoreText->setPlainText(QString("Score: ") + QString::number(impl_->score));
}
