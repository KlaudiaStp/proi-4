#-------------------------------------------------
#
# Project created by QtCreator 2019-06-01T11:05:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pacman
TEMPLATE = app


SOURCES += main.cpp \
    Game.cpp \
    Pacman.cpp \
    Fruit.cpp \
    Ghost.cpp \
    Blinky.cpp \
    Pinky.cpp \
    Inky.cpp \
    Clyde.cpp

HEADERS  += \
    Game.h \
    Pacman.h \
    Fruit.h \
    Ghost.h \
    Blinky.h \
    Pinky.h \
    Inky.h \
    Clyde.h

FORMS    +=

CONFIG += c++11

RESOURCES += \
    res.qrc
