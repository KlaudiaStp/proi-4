#ifndef FRUIT_H
#define FRUIT_H
#include <QGraphicsPixmapItem>
#include "Game.h"

class Fruit: public QGraphicsPixmapItem
{
public:
    Fruit(int x, int y, int type);
    int getType();

private:
    int type;
};

#endif // FRUIT_H
