#ifndef GHOST_H
#define GHOST_H
#include <QObject>
#include <QGraphicsPixmapItem>
#include "Game.h"
#include <QTimer>
#include "Pacman.h"

class Ghost : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Ghost(Game *g);
public slots:
    virtual void move()=0;
protected:
    int posX;
    int posY;
    int counter;
    Game *game;
    QTimer *timer;
    void print();
    void moveTo(int x, int y);
    void checkCollids();

};

#endif // GHOST_H
