#ifndef GAME_H
#define GAME_H
#include <QGraphicsView>
#include <QGraphicsScene>
#include <memory>
#define BOARDW 24
#define BOARDH 16
#define CELLW 40
#define CELLH 40
#define MAX_LEVEL 2
#define SCENEW BOARDW*CELLW
#define SCENEH (BOARDH+1)*CELLH
#include "Fruit.h"
#include <QMessageBox>
#include <QCoreApplication>
#include <exception>
#include <QGraphicsTextItem>
#include <QList>



class Game : public QGraphicsView
{
public:
    Game();
    QGraphicsScene *scene;
    void play();
    bool canMove(int x, int y);
    void applyPacmanMove(int type);
    void applyGhostMove (QGraphicsPixmapItem *g);
    int getPacmanX();
    int getPacmanY();

private:
    struct GameImpl;
    std::unique_ptr<GameImpl> impl_;
    void gameSet ();
    void changeDisp();
};

#endif // GAME_H
