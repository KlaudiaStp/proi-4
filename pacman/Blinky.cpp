#include "Blinky.h"

Blinky::Blinky(Game *g, int x, int y) : Ghost(g)
{
    setPixmap(QPixmap(":/images/gh1.ico").scaled(CELLW,CELLH));
    posX=x;
    posY=y;
    print();
}

void Blinky::move()
{
    checkCollids();
    counter=(counter+1)%4;
    if(counter==0)
    {   int tmpX=posX;
        int tmpY=posY;
        //int nm=rand()%4;
        int tab[4];
        for(int i=0; i<4; ++i)
        {
            tmpX=posX;
            tmpY=posY;
        switch (i)
        {
        case 0: ++tmpX;
            break;
        case 1: ++tmpY;
            break;
        case 2: --tmpX;
            break;
        case 3: --tmpY;
            break;
        }
        tab[i]=(tmpX-game->getPacmanX())*(tmpX-game->getPacmanX())+(tmpY-game->getPacmanY())*(tmpY-game->getPacmanY());
        }
        int dir=0;
        for(int i=0; i<4; ++i)
        {

            if(tab[i]<tab[dir])
            {
                dir=i;
            }
        }
        bool ok=false;
        while(!ok)
        {
            tmpX=posX;
            tmpY=posY;
            switch (dir)
            {
            case 0: ++tmpX;
                break;
            case 1: ++tmpY;
                break;
            case 2: --tmpX;
                break;
            case 3: --tmpY;
                break;
            }
            if(game->canMove(tmpX, tmpY))
            {
                moveTo(tmpX, tmpY);
                ok=true;

            }
            else
              dir=(dir+1)%4;
        }


    }

}
