#include "Pinky.h"


Pinky::Pinky(Game *g, int x, int y) : Ghost(g)
{
    setPixmap(QPixmap(":/images/gh2.ico").scaled(CELLW,CELLH));
    posX=x;
    posY=y;
    print();
}

void Pinky::move()
{
    checkCollids();
    counter=(counter+1)%4;
    if(counter==0)
    {
        int nm=rand()%4;
        int tmpX=posX;
        int tmpY=posY;

        switch (nm)
        {
        case 0: ++tmpX;
            break;
        case 1: ++tmpY;
            break;
        case 2: --tmpX;
            break;
        case 3: --tmpY;
            break;
        }
        if(game->canMove(tmpX, tmpY))
        {
            moveTo(tmpX, tmpY);
        }
    }

}
