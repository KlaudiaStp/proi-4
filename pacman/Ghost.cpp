#include "Ghost.h"

Ghost::Ghost(Game *g) : QObject(), QGraphicsPixmapItem()
{
    game =g;
    timer = new QTimer (this);
    connect(timer, SIGNAL(timeout()), this, SLOT(move()));
    timer->start(200);
    counter=0;

}

void Ghost::print()
{
    setPos(posX*CELLW, posY*CELLH);
}

void Ghost::moveTo(int x, int y)
{
    posX=x;
    posY=y;
    print();
}

void Ghost::checkCollids()
{
    QList<QGraphicsItem*> collids = collidingItems();
    for(int i=0; i<collids.size(); i++)
    {
        if(typeid(*(collids[i]))==typeid(Pacman))
        {
            game->applyGhostMove(this);
        }
    }
}


