#include "Fruit.h"

Fruit::Fruit(int x, int y, int type): QGraphicsPixmapItem()
{   this->type=type;
    if (type==1)
    {
    setPixmap(QPixmap(":/images/red.png").scaled (20, 20));
    setPos(x*CELLW, y*CELLW);
    }
    else
    {
        setPixmap(QPixmap(":/images/1.jpg").scaled (30, 30));
        setPos(x*CELLW, y*CELLW);
    }
}

int Fruit::getType()
{
    return type;
}
