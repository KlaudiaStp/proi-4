#ifndef BLINKY_H
#define BLINKY_H
#include "Ghost.h"

class Blinky : public Ghost
{
public:
    Blinky(Game *g, int x, int y);
    virtual void move();

};

#endif // BLINKY_H
