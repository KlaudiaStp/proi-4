#include "Pacman.h"
#include "Fruit.h"

Pacman::Pacman(Game *g):QObject(), QGraphicsPixmapItem()
{
    setPixmap(QPixmap(":/images/pacman.png").scaled(CELLW, CELLH));
    posX=0;
    posY=0;
    print();
    setFlag(QGraphicsItem::ItemIsFocusable);
    setFocus();
    game=g;
    direction=0;
    timer = new QTimer (this);
    connect(timer, SIGNAL(timeout()), this, SLOT(move()));
    timer->start(400);


}

void Pacman::keyPressEvent(QKeyEvent *event)
{   setTransformOriginPoint(CELLW/2, CELLH/2);
    if(event->key()==Qt::Key_Left)
    {
        setRotation(-180);
        direction=2;
    }
    else if(event->key()==Qt::Key_Right)
    {
        setRotation(0);
        direction=0;
    }
    else if(event->key()==Qt::Key_Down)
    {
        setRotation(90);
        direction=1;
    }
    else if(event->key()==Qt::Key_Up)
    {
        setRotation(-90);
        direction=3;
    }
}

void Pacman::moveTo(int x, int y)
{
    posX=x;
    posY=y;
    print();
}

int Pacman::getX()
{
    return posX;
}

int Pacman::getY()
{
    return posY;
}

void Pacman::move()
{
    int tmpX=posX;
    int tmpY=posY;

    switch (direction)
    {
    case 0: ++tmpX;
        break;
    case 1: ++tmpY;
        break;
    case 2: --tmpX;
        break;
    case 3: --tmpY;
        break;
    }
    if(game->canMove(tmpX, tmpY))
    {
        moveTo(tmpX, tmpY);
        QList<QGraphicsItem*> collids = collidingItems();
        for(int i=0; i<collids.size(); i++)
        {
            if(typeid(*(collids[i]))==typeid(Fruit))
            {
                scene()->removeItem(collids[i]);
                int t=((Fruit*)collids[i])->getType();
                delete collids[i];
                game->applyPacmanMove(t);

            }
        }
    }

}

void Pacman::print()
{
    setPos(posX*CELLW, posY*CELLH);
}
