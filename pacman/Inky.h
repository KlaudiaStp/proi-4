#ifndef INKY_H
#define INKY_H
#include "Ghost.h"

class Inky : public Ghost
{
public:
    Inky(Game *g, int x, int y);
    virtual void move();

};
#endif // INKY_H
