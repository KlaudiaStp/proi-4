#ifndef PINKY_H
#define PINKY_H
#include "Ghost.h"

class Pinky : public Ghost
{
public:
    Pinky(Game *g, int x, int y);
    virtual void move();

};

#endif // PINKY_H
